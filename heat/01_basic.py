
import os

from dolfin.function.functionspace import FunctionSpace
from dolfin.function.argument import TestFunction
from dolfin.function.argument import TrialFunction
from dolfin.function.function import Function
from dolfin.function.constant import Constant
from dolfin.function.expression import Expression

from dolfin.fem.dirichletbc import DirichletBC
from dolfin.fem.solving import solve

from dolfin.cpp.io import XDMFFile

from ufl import grad
from ufl import dx
from ufl import dot

from fenics_utils.mesh.external import load_dolfin_mesh


T = 0.01  # final time
num_steps = 20  # number of time steps
dt = T / num_steps  # time step size
var_name = 'Temperature'


mesh_dir = '_meshes'
mesh_filename = os.path.join(mesh_dir, 'trappedvtx.xdmf')


outputs_dir = '_outputs'
sol_name = 'example_parallel'
sol_filename = os.path.join(outputs_dir, f'{sol_name}.xdmf')


mesh, boundary_markers, patch_labels = load_dolfin_mesh(mesh_filename)

V = FunctionSpace(mesh, 'Lagrange', 1)

u_inlet, u_outlet = Constant(300.), Constant(300.)
bcs = [DirichletBC(V, u_inlet, boundary_markers, 0),
       DirichletBC(V, u_outlet, boundary_markers, 1)]

c = 0.1
param = u_inlet.values()[0] / (c**2)

u_initial = Expression('param * (x[0] - c) * (x[0] - c)',
                       degree=2, param=param, c=c, name=var_name)


u_n = Function(V, name=var_name)
u_n.interpolate(u_initial)


u_h = TrialFunction(V)
v_h = TestFunction(V)
f = Constant(0.)

a = u_h * v_h * dx + dt * dot(grad(u_h), grad(v_h)) * dx
L = (u_n + dt * f) * v_h * dx

solver_parameters = {'linear_solver': 'cg',
                     'preconditioner': None,
                     'symmetric': True, }


file = XDMFFile(sol_filename)
file.parameters['flush_output'] = False
file.parameters['functions_share_mesh'] = True
file.parameters['rewrite_function_mesh'] = False

t = 0.

file.write(u_n, t)

u = Function(V, name=var_name)
for n in range(num_steps):

    # update curent time
    t += dt

    # compute solution
    solve(a == L, u, bcs, solver_parameters=solver_parameters)

    # save to file
    file.write(u, t)

    # update previous solution
    u_n.assign(u)

file.close()
