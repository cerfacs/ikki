
doc:
	cd docs && make html

clean:
	rm -rf docs/_build
	rm -rf docs/source/api/_generated