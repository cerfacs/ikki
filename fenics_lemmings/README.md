# `fenics`-`lemmings-hpc` interaction: the serial case

This example shows how to run serial simulations in `fenics` (time-dependent heat equation problem in a square) via `lemmings-hpc`. `lemmings-hpc` allows to easily automate the process of restarting a simulation. The need to restart a simulation can come from the wall time constraints normally imposed on HPC clusters. This setup is intended to run on **Kraken** (setting everything up in a different machine should be straightforward).


## Set up a virtual environment

A good practice is to set up a virtual environment:

```bash
python3 -m venv lemmings_env
```

`venv` creates a Python virtual environment with the same version as the Python installation you're using (try out `python3 --version`). Remember `module av` provides a way to check all the available software. For example, we chose to use `python/3.7.7` (`module load python/3.7.7`).


To activate the virtual environment:

```bash
source lemmings_env/bin/activate
```

To install `lemmings-hpc`:

```bash
pip install pip --upgrade
pip install lemmings-hpc==0.2.3a0
```

Notice we've specified the version of `lemmings-hpc` to install. This ensure you will not have surprises when running this code. Nevertheless, you can try out more recent versions of the package and verify if it still runs (if not, try to figure out what has changed!).


## Set up `lemmings-hpc`

`lemmings-hpc` requires two files: a `yaml` file (for parameter specification) and a Python file (where you can put `LemmingJobBase` on steroids, i.e. add behavior that is particular to your use case). Check them out for the serial case ([`yaml`](https://nitrox.cerfacs.fr/open-source/ikki/blob/master/fenics_lemmings/serial/fenics_recursive.yml) and [`py`](https://nitrox.cerfacs.fr/open-source/ikki/blob/master/fenics_lemmings/serial/fenics_recursive.py)).


There's a lot of things you can control in the `yaml` file, the most relevant for this example are:

* `exec` and `exec_pj`: where you write bash script code to launch the simulation and perform post-job tasks.

* `job_queue` and `pjob_queue`: where you set your SLURM specifications based on your machine template.

* `cpu_limit`: a required parameter, we set it to 1 (CPUh).

* `custom_params`: where you define variables that can be accessed within your `LemmingsJob` class via `self.machine.user.custom_params[<param_name>]`.


An important step is the set up of an environment variable that points to your [machine template](https://nitrox.cerfacs.fr/open-source/ikki/blob/master/fenics_lemmings/kraken.yml) (example machine templates can be found in `lemmings-hpc` installation files: `<path_to_site-packages>/lemmings_hpc/chain/machine_template`). To create an environment variable:

```bash
export LEMMINGS_MACHINE='<path_to_machine_template>'
```


For example:

```bash
export LEMMINGS_MACHINE='/scratch/cfd/pereira/ikki/fenics_lemmings/kraken.yml'
```

The environment variable will live only in your current session. To make it persist, you have to add it directly in, for instance, your `.bash_profile`.


Within your machine template you can create several queues (which can then be assigned to your job in `lemmings-hpc` configuration file). In the present serial run example we use a `debug` and a `debug_pj` queue which each use a single core.


Within the Python file, you should inherit from `LemmingsJobBase` (import it from `lemmings_hpc.chain.lemmingjob_base`) and define the specific behavior for your runs. We chose to specify two particular behaviours:

* `prior_to_job`: ensures the output directory exists and is empty (before launching any simulation).

* `check_on_end`: at the end of each run we check how many iterations were performed and decide if it is time to stop.


## Set up `fenics` run

This is a simple example, so there's not much flexibility in `fenics` part. Some parameters can be controlled through a [`json` file](https://nitrox.cerfacs.fr/open-source/ikki/blob/master/fenics_lemmings/serial/restart_example.json). The most important for this example is `max_runtime`, which sets the maximum allowed time for each run.

The `fenics` implementation used here relies on two open-source libraries (also developed at Cerfacs) that contain useful utilities to deal with `fenics` and numerical simulations. Since the [singularity](https://sylabs.io/singularity/) image available in Kraken is used to run `fenics`, these external libraries cannot be installed (which justifies they're existence [here](https://nitrox.cerfacs.fr/open-source/ikki/tree/master/fenics_lemmings/serial/fenics_utils) and [here](https://nitrox.cerfacs.fr/open-source/ikki/tree/master/fenics_lemmings/serial/numerical_utils)). If you find any bug, please make changes directly in the corresponding repos, that can be found [here](https://github.com/lpereira95/fenics_utils) and [here](https://github.com/lpereira95/numerical_utils) (these libraries are not stable, so be careful if you use them directly from source).



## Run chain

Now, it is time to run the chain (your working directory must be `serial` and `lemmings_env` has to be activated):

```bash
lemmings-hpc run fenics_recursive
```

As a safety, the user will be required to confirm the allowed CPUh which is specified in the `fenics_recursive.yml` file.  Once this is done `lemmings-hpc` will start.

If something goes wrong (let's hope not!), you can kill the submitted jobs via:

```bash
lemmings-hpc kill
```


If you need to clean the working directory, just do (be careful because the outputs folder will also be removed):

```bash
sh clean.sh
```
Note: you could also rely on the 
```bash
lemmings-hpc clean
```
command which will remove the files generated by lemmings-hpc, but not the user's or sheduler's generated ones.


To verify the jobs you're running:

```bash
squeue -u <username>
```
Note that this is the command for a slurm job sheduler and can be different on your machine.

You can also rely on 
```bash
lemmings-hpc status
```
to get more info about your simulation. 

The run creates a directory (`outputs` if you don't change the configuration file) that contains three files: two of them (`.xdmf` and `.h5`) store simulation data and the other (`.json`) contains information about the running time (you can access the time each iteration took, as well as the number of restarts).

The `xdmf` file [cannot be directly read in paraview](https://fenicsproject.discourse.group/t/loading-xdmf-data-back-in/1925) (as it contains the information required to reload data back in `fenics`, whereas paraview requires the values at the vertices). Our workaround is to convert this file to a format amenable to paraview (`vkt` in this case). Alternatively, `xdmf` could be used if method `write` is used instead of `write_checkpoint`. Here the steps for the conversion:

```python
import os

from dolfin.cpp.generation import UnitSquareMesh
from dolfin.function.functionspace import FunctionSpace

from fenics_utils.utils import convert_xdmf_checkpoints_to_vtk

var_name = 'Temperature'
mesh = UnitSquareMesh(n, n)
V = FunctionSpace(mesh, 'Lagrange', 1)

xdmf_filename = os.path.join('outputs', 'example.xdmf')
convert_xdmf_checkpoints_to_vtk(xdmf_filename, V, var_name)
```



Voilà, you are now able to successfully use `fenics` within `lemmings-hpc` framework and run your simulations on steroids!

