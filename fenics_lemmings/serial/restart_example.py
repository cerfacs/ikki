import os
import json

from fenics import MPI

from dolfin.cpp.generation import UnitSquareMesh
from dolfin.cpp.fem import LinearVariationalSolver

from dolfin.fem.solving import LinearVariationalProblem
from dolfin.function.functionspace import FunctionSpace

from fenics_utils.setups.heat import set_linear_equal_opposite
from fenics_utils.time_dependent.solving import StopByItersOrRuntime
from fenics_utils.time_dependent.solving import solve_time_dependent_with_restart

from numerical_utils.timers import TimerForIterations as Timer


# read inputs
inputs_filename = 'restart_example.json'
with open(inputs_filename, 'r') as file:
    params = json.load(file)

dt = params['dt']
n = params['n']
max_iters = params['max_iters']
max_runtime = params['max_runtime']
outputs_dir = params['outputs_dir']
sim_name = params['sim_name']
solver_params = params['solver_params']

# other parameters definition
var_name = 'Temperature'


xdmf_filename = os.path.join(f'{outputs_dir}', f'{sim_name}.xdmf')
timer_filename = os.path.join(f'{outputs_dir}', f'{sim_name}_timer.json')
comm = MPI.comm_world
if comm.Get_size() > 1:
    rank = comm.Get_rank()
    xdmf_filename = xdmf_filename.split('.')[0] + f'_{rank}.xdmf'
    timer_filename = timer_filename.split('.')[0] + f'_{rank}.json'


# set problem
mesh = UnitSquareMesh(n, n)
V = FunctionSpace(mesh, 'Lagrange', 1)
u_n, a, L, u, bcs = set_linear_equal_opposite(mesh, dt, var_name=var_name, V=V)
problem = LinearVariationalProblem(a, L, u, bcs)

# solve
solver = LinearVariationalSolver(problem)
solver.parameters.update(solver_params)

stop_criterion = StopByItersOrRuntime(max_iters, max_runtime)

timer = Timer().load(timer_filename) if os.path.exists(timer_filename) else Timer()

is_finished = solve_time_dependent_with_restart(
    xdmf_filename, V, var_name, u_n, u, solver, dt, timer,
    stop_criterion)

timer.dump(timer_filename)
