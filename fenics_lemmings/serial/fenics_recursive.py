"""
Lemmings script for an AVBP recursive simulation
"""

import os
import shutil
import json

from lemmings_hpc.chain.lemmingjob_base import LemmingJobBase

# pylint: disable=invalid-name


class LemmingJob(LemmingJobBase):
    # pylint: disable=too-many-instance-attributes
    # pylint: disable=logging-not-lazy
    """
    A lemming job follows always the same pattern.
    START > SPAWN JOB> POST JOB > SPAWN JOB > POST JOB > EXIT

    each step can be customized in the present class.
    e.g. you control the nb. of SPAWN JOB>POST JOB with the 'Check on end` function.


                 Prior to job  +---------+             Prepare run
                     +--------->SPAWN JOB+---------------------+
                     |         +------^--+                     |
                     |                |                      +-v------+
                   True               |                      |POST JOB|
    +-----+          |                |                      +--------+
    |START+--->Check on start         |                          v
    +-----+          |                +---------------False-Check on end
                   False            Prior to new iteration       +
                     |                                         True
                     |                                           |
                     |                                           |
                     |           +----+                          |
                     +---------->|EXIT|<-------------------------+
               Abort on start    +----+                After end job

    you can use the database if you need to store info from one job to the other.

    """

    def prior_to_job(self):
        """
        Function that prepares the run when the user launches the Lemmings command.
        """

        # load params
        params = self._load_params()
        outputs_dir = params['outputs_dir']

        # define directory where outputs are located for database.yml
        self.database.update_current_loop('solut_path', outputs_dir)

        if os.path.exists(outputs_dir):
            shutil.rmtree(outputs_dir)
        os.mkdir(outputs_dir)

    def check_on_end(self):
        """
        Verify after each loop job executed  if:
            - the cpu condition is reached: evaluated by lemmings
            - the run has crashed: evaluated by user
            - the time condition is satisfied: evaluated by user

        The function check_on_end needs to return a boolean with three options:
            - False: we continue lemmings
            - True: target reached, we stop lemmings
            - None: crash, we stop lemmings
        """

        # load params
        params = self._load_params()

        outputs_dir = params['outputs_dir']
        sim_name = params['sim_name']
        timer_filename = os.path.join(f'{outputs_dir}', f'{sim_name}_timer.json')
        max_iters = params['max_iters']

        # check max iterations
        timer_filename = os.path.join('outputs', f'{sim_name}_timer.json')

        with open(timer_filename, 'r') as file:
            timer_data = json.load(file)

        it = len(timer_data['iter_time'])

        return it == max_iters

    def _load_params(self):
        # get input filename
        inputs_filename = self.machine.user.custom_params['inputs_filename']

        # get simulation params from inputs file
        with open(inputs_filename, 'r') as file:
            params = json.load(file)

        return params
