rm batch*
rm slurm*
rm database.yml
rm -r fenics_example*
rm -r  __pycache__
rm outputs/*
rm log.out
