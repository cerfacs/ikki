# IKKI : repo for Fenics at Cerfacs.

This is the collection of tools we created in our experiences on Fenics at Cerfacs.
Find the documentation of this project on [readthedocs](https://fenics-cerfacs.readthedocs.io/en/latest/).

## Video


In March 2022, LF Pereira held a seminar on this work.
You can view it on [youtube]( https://youtu.be/nhkXMBtQNfo).


Abstract:
"Over the last year, at COOP, we've started using FEniCS to solve 
partial differential equations as part of our technology survey role.
We have tackled different kind of problems, such as simple linear equations 
(e.g. Poisson and heat equation), eigenvalue problems (Helmholtz equation),
and more complex systems of equations (Navier-Stokes equation 
coupled with an advection equation - in collaboration with GLOBC team).

While solving these problems, we've developed a set of utils that aim 
to simplify the use of FEniCS for users already acquainted with 
the typical CERFACS workflow (e.g. `hip` meshes can be used seamlessly in FEniCS).
Besides, we've looked for scalable solutions 
(something usually dismissed in FEniCS-related resources) and, when possible, 
we've benchmarked our solvers against existing in-house solvers
(in particular, AVTP and AVSP).

We've found out that FEniCS can reach a very wide spectrum of users:
from the more physics-oriented (which mostly expect it to be a black-box solver),
to the more mathematically-oriented (which mostly intend to use it to assemble matrices).
In this talk, we will share the knowledge we've acquired and raise awareness
of some FEniCS quirks that can hugely influence the 
performance of FEniCS-based solvers."


