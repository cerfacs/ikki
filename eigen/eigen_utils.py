
import numpy as np

from dolfin.function.argument import TrialFunction
from dolfin.function.argument import TestFunction
from dolfin.cpp.la import PETScMatrix
from dolfin.cpp.la import SLEPcEigenSolver
from dolfin.cpp.common import DOLFIN_EPS
from dolfin.fem.assembling import assemble

from ufl import inner
from ufl import grad
from ufl import dx


def formulate_laplacian(V):

    u_h = TrialFunction(V)
    v = TestFunction(V)

    a = inner(grad(u_h), grad(v)) * dx
    b = inner(u_h, v) * dx

    return a, b


def set_generic(V, bcs=None, diag_value=1e6):
    """
    Args:
        bcs_fnc (callable): first argument must be V.
        kwargs: passed to bcs_fnc.
    """

    a, b = formulate_laplacian(V)

    # assemble matrices
    A = PETScMatrix(V.mesh().mpi_comm())
    assemble(a, tensor=A)

    B = PETScMatrix(V.mesh().mpi_comm())
    assemble(b, tensor=B)

    if bcs is None:
        return A, B

    # collect bcs dofs
    bc_dofs = []
    for bc in bcs:
        bc_dofs.extend(list(bc.get_boundary_values().keys()))

    # apply bcs
    A.mat().zeroRowsColumnsLocal(bc_dofs, diag=diag_value)
    # TODO: need to apply in B?
    B.mat().zeroRowsColumnsLocal(bc_dofs, diag=1.)

    return A, B


class MySLEPcEigenSolver(SLEPcEigenSolver):

    def __init__(self, A, B, solver='generalized-davidson',
                 spectrum='smallest magnitude', problem_type='gen_hermitian',
                 comm=None):

        if comm is None:
            super().__init__(A, B)
        else:
            super().__init__(comm)
            self.set_operators(A, B)

        self.parameters['solver'] = solver
        self.parameters['spectrum'] = spectrum
        self.parameters['problem_type'] = problem_type
        self.parameters['tolerance'] = DOLFIN_EPS

    def solve(self, n_eig=5):
        super().solve(n_eig)

        return collect_SLEPc_eigenpairs(self)


def collect_SLEPc_eigenpairs(solver):
    """Returns only real part.
    """

    w, v = [], []
    for i in range(solver.get_number_converged()):
        r, _, rv, _ = solver.get_eigenpair(i)
        w.append(r)
        v.append(rv)

    return np.array(w), np.array(v).T
