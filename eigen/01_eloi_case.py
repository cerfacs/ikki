
import os
import time

# ensure OMP do not slow down the code (need to be before external imports)
os.environ['OMP_NUM_THREADS'] = '1'

import numpy as np

from dolfin.function.function import Function
from dolfin.function.functionspace import FunctionSpace
from dolfin.cpp.io import XDMFFile
from dolfin.cpp.geometry import Point
from dolfin.cpp.generation import BoxMesh
from dolfin.cpp.math import near
from dolfin.cpp.common import DOLFIN_EPS
from dolfin.fem.dirichletbc import DirichletBC

from eigen_utils import set_generic
from eigen_utils import MySLEPcEigenSolver as SLEPcEigenSolver


N = 100

n_eig = 20
solver_method = 'generalized-davidson'
tol = 1e-8

c0 = 840.
rho = 1.

outputs_dir = '_outputs'
sol_name = 'example_parallel'
sol_filename = os.path.join(outputs_dir, f'{sol_name}.xdmf')


dims = [0.2, 0.1, 1.0]
n = [int(N * dim) for dim in dims]
d = len(n)
origin = Point([0.] * d)
point = Point(dims)
mesh = BoxMesh(origin, point, *n)

V = FunctionSpace(mesh, 'P', 1)


def boundary_z(x, on_boundary):
    return on_boundary and (near(x[2], 0., DOLFIN_EPS) or near(x[2], dims[2], DOLFIN_EPS))


bcs = [DirichletBC(V, 0., boundary_z)]


A, B = set_generic(V, bcs=bcs)


if V.mesh().mpi_comm().rank == 0:
    print(f'number of nodes: {A.size(0)}')


solver = SLEPcEigenSolver(A, B, comm=V.mesh().mpi_comm())
solver.parameters['solver'] = solver_method
solver.parameters['tolerance'] = tol

start_time = time.perf_counter()
w, v = solver.solve(n_eig)


print(f'Total time: {time.perf_counter()-start_time} s')

if V.mesh().mpi_comm().rank == 0:
    print(f'How many converged? {solver.get_number_converged()}')


# save solution
file = XDMFFile(sol_filename)
file.parameters['flush_output'] = False
file.parameters['functions_share_mesh'] = True
file.parameters['rewrite_function_mesh'] = False

mode = Function(V, name='mode')


for i, eigenvalue in enumerate(w):
    mode.vector()[:] = v[:, i]

    f = np.sqrt(eigenvalue * rho) * c0 / (2 * np.pi)
    file.write(mode, f)


file.close()
