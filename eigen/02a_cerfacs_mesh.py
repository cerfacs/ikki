
import os
import time

# ensure OMP do not slow down the code (need to be before external imports)
os.environ['OMP_NUM_THREADS'] = '1'


from dolfin.function.functionspace import FunctionSpace
from dolfin.fem.dirichletbc import DirichletBC
from dolfin.function.function import Function
from dolfin.cpp.io import XDMFFile
from dolfin.cpp.math import near
from dolfin.cpp.common import DOLFIN_EPS
from dolfin.cpp.generation import UnitSquareMesh

from eigen_utils import set_generic
from eigen_utils import MySLEPcEigenSolver as SLEPcEigenSolver


n_eig = 20
solver_method = 'generalized-davidson'
tol = 1e-8

outputs_dir = '_outputs'
sol_name = 'example_parallel'
sol_filename = os.path.join(outputs_dir, f'{sol_name}.xdmf')


n = 9
mesh = UnitSquareMesh(n, n)

V = FunctionSpace(mesh, 'P', 1)


def boundary_xmin(x, on_boundary):
    return on_boundary and near(x[0], 0., DOLFIN_EPS)


bcs = [DirichletBC(V, 0., boundary_xmin)]

A, B = set_generic(V, bcs)


if V.mesh().mpi_comm().rank == 0:
    print(f'number of nodes: {A.size(0)}')


solver = SLEPcEigenSolver(A, B, comm=V.mesh().mpi_comm())
solver.parameters['solver'] = solver_method
solver.parameters['tolerance'] = tol

start_time = time.perf_counter()
w, v = solver.solve(n_eig)


print(f'Total time: {time.perf_counter()-start_time} s')

if V.mesh().mpi_comm().rank == 0:
    print(f'How many converged? {solver.get_number_converged()}')


# save solution
file = XDMFFile(sol_filename)
file.parameters['flush_output'] = False
file.parameters['functions_share_mesh'] = True
file.parameters['rewrite_function_mesh'] = False

mode = Function(V, name='mode')


for i, eigenvalue in enumerate(w):
    mode.vector()[:] = v[:, i]

    file.write(mode, eigenvalue)


file.close()
