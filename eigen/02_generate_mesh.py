
import os

from pyhip.commands.operations import hip_exit
from pyhip.commands.operations import generate_mesh_2d_3d
from pyhip.commands.writers import write_hdf5

import yamio

# create mesh with hip
mesh_dir = '_meshes'
lower_corner = (0., 0.)
upper_corner = (1., 1.)
resolution = (10, 10)
convert2tri = True

mesh_filename = os.path.join(mesh_dir, 'square_tri')

generate_mesh_2d_3d(lower_corner, upper_corner, resolution,
                    convert2tri=convert2tri)
write_hdf5(mesh_filename)

hip_exit()


# read mesh in yamio
mesh = yamio.read(f'{mesh_filename}.mesh.xmf')

# write format compatible with fenics
dolfin_filename = f'{mesh_filename}.xdmf'
mesh.write(dolfin_filename, file_format='dolfin-yamio')
