
.. _how_to:

=============
How-To Guides
=============


.. toctree::
    :maxdepth: 1

    solver_choice    
    convert_cerfacs_mesh
    basic_bcs
    bcs_from_patches


    
.. todo::
    
    * compile functions