

.. |fenics| replace:: FEniCS


.. _bcs-from-patches:

How to define Dirichlet boundary conditions using patches?
==========================================================


When we use ``hip`` meshes, we have information about boundaries stored in patches (check out :ref:`convert-cerfacs-mesh` to find out how to load this information in |fenics|). Defining Dirichlet boundary conditions using these patches is very similar to the case where we use a function to identify the boundary (see :ref:`this <define-bc>` example), but instead we pass the boundary markers and the number corresponding to the patch.


.. code-block:: python

    from dolfin.fem.dirichletbc import DirichletBC

    bc = DirichletBC(V, u_D, boundary_markers, 0)


The index is given by the ordering of patch labels.

