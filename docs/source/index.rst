=================
FEniCS at CERFACS
=================

**Date**: |today|


.. |fenics| replace:: FEniCS


These notebooks present some of the on-going experiments made with |fenics| at CERFACS. Here, we try to show not only how to solve problems, but also the lessons we've learned along the way.



.. toctree::
    :maxdepth: 2

    intro/index
    tutorials/index
    how_to/index
