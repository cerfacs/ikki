

Benchmarking against AVTP
=========================


.. todo::

    * complete case description: outlet Dirichlet 1300K, everything else 300K, fixed timestep
    * show how to run FEniCS in kraken (make a how to)
    * speak about conda problems + fenics problems when too little dof per cpu



.. todo::

    * results are outdated: new fenics version is much faster
    * design better the benchmarks




For a coarse mesh (~150k cells):


.. image:: images/benchmark_coarse.png
    :align: center



For a fine mesh (~10M cells):

.. image:: images/benchmark_refined.png
    :align: center


