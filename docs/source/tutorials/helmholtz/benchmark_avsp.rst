

Benchmarking against AVSP
=========================

.. todo::

    * a more complete benchmark must be done, the results serve only as reference


For a mesh with around 120k nodes on both FEniCS and ``AVSP``:

 ======== ========= ======= 
  Cores    FEniCS    AVSP   
 ======== ========= ======= 
  1        <20s      17s    
  8        -         2s     
  16       -         1s     
  36       -         1s     
 ======== ========= =======