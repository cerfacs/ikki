
.. |fenics| replace:: FEniCS


Helmholtz equation in a manifold
================================


Without any additional effort, we can use our :ref:`Helmholtz solver <helmholtz>` in a manifold (see `Automating the solution of PDEs on the sphere and other manifolds in FEniCS 1.2 <https://gmd.copernicus.org/articles/6/2099/2013/>`_).


Let's start by creating a shell using `kokiy <https://pypi.org/project/kokiy/>`_:


.. code-block:: python

    from kokiy.axishell import AxiShell


    ctrl_pts_x = (0.0, 0.2)
    ctrl_pts_r = (0.12, 0.12)
    angle = 60
    n_azi, n_long = 8, 10

    shell = AxiShell(n_azi, n_long, angle, ctrl_pts_x, ctrl_pts_r)


Then, we can simply export it directly to a |fenics| readable format:

.. code-block:: python

    shell.export_mesh(mesh_filename, 'triangle')


And read it back in |fenics|:

.. code-block:: python

    from dolfin.cpp.mesh import Mesh
    from dolfin.cpp.io import XDMFFile

    mesh = Mesh()
    with XDMFFile(mesh_filename) as file:
        file.read(mesh)


Now, simply following the same steps as :ref:`here <helmholtz>`, we can get the eigenvalues and modes (check some examples below).


.. image:: images/kokiy_1.png
    :align: center
    :scale: 40%

|

.. image:: images/kokiy_2.png
    :align: center
    :scale: 40%

|

.. image:: images/kokiy_3.png
    :align: center
    :scale: 40%

|

.. image:: images/kokiy_5.png
    :align: center
    :scale: 40%

|