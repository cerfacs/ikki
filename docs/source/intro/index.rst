

.. |fenics| replace:: FEniCS
.. _fenics : https://fenicsproject.org/

.. |petsc| replace:: PETSc
.. _petsc : https://www.mcs.anl.gov/petsc/


.. _intro:

============
Introduction
============


|fenics|_ is the project name for a set of computational components, which include libraries such as DOLFIN, UFL (unified form language, which is a special language for implementing the variational formulation of a problem), FFC (FEniCS form compiler), UFC (unified form-assembly code) and FIAT (finite element automatic tabulator). You can find the open-source repositories `here <https://bitbucket.org/%7B795b0eed-436d-4242-83d7-a46a0b061354%7D>`_.

In a nutshell, FFC transforms UFL language into the corresponding C++ interface code (UFC). FIAT contains finite element basis functions and the corresponding quadrature rules. DOLFIN contains the data structures, assembly routines and linear solvers. 


The next diagram, although slightly outdated (e.g. FErari is deprecated), gives an excellent overview of the ecosystem.


.. image:: images/fenics_ecossystem.png
    :align: center


The normal user interacts directly only with DOLFIN and UFL. DOLFIN is written is C++, but wrapped in Python. UFL is pure Python code.

DOLFIN interfaces with several linear algebra packages, known in |fenics| lingo as *linear algebra backends*. By default, |petsc|_ is used. |petsc| supports MPI, GPUs through CUDA or OpenCL and hybrid MPI-GPU parallelism (DOLFIN only support MPI). This reliance on external solvers should not be overlooked: it allows the use of state-of-the-art solvers with minimal effort, while being extremely flexible. If you are brave enough and really want to use your own linear algebra library, you can probably change the source code to interact with it (that's the beauty of open-source software).

To check which are the backends available in your installation, just do:


.. code-block:: python

    from dolfin.cpp.la import list_linear_algebra_backends

    list_linear_algebra_backends()


You can also get more information about the solvers and preconditioners by doing:


.. code-block:: python

    from dolfin.cpp.la import list_lu_solver_methods
    from dolfin.cpp.la import list_krylov_solver_methods
    from dolfin.cpp.la import list_krylov_solver_preconditioners

    list_lu_solver_methods()
    list_krylov_solver_methods()
    list_krylov_solver_preconditioners()


This `page <https://fenicsproject.org/download>`_ shows how to install |fenics| in your machine. The easiest way for a Python regular user is to rely on Anaconda (Linux and MacOS only):


.. code-block:: bash

    conda install fenics


Recently, |fenics| core libraries started undergoing a major redevelopment. That's why you may have found information about `FEniCS-X <https://jorgensd.github.io/dolfinx-tutorial/index.html>`_ in your searches. These new libraries should be used in the future, as the old project will "see much less development and less maintenance". Nevertheless, since the new project was officially released only very `recently <https://github.com/FEniCS/dolfinx/releases>`_, this tutorial still uses the old libraries. You can find out everything about it `here <https://fenicsproject.org/fenics-project-roadmap-2019>`_. The new libraries repositories can be found `here <https://github.com/FEniCS>`_.



.. toctree::
    :maxdepth: 1
    :hidden:

    references
    tips
