

Tips
====

Here, a list of useful tips/remarks that, hopefully, will make your life easier.

* The choice of the solver can have huge impact on performance (see :ref:`solver-choice`).


* If a given matrix is required in several steps, it should be assembled only once (e.g. time-dependent problems).


* Some functions don't have the expected behavior within a ``jupyter notebook``. This applies mainly to functions imported from ``dolfin.cpp`` which task is to print information (e.g. ``dolfin.cpp.la.list_linear_algebra_backends``). This does not mean you should stop using ``jupyter notebook`` (they're very useful!), it simply means you have to go for a script to get this kind of information.


* Code verbosity can change: ``set_log_level(log_level)``. In old documentation, you can find ``set_log_level(DEBUG)`` and ``set_log_level(PROGRESS)`` as the go-to commands. Nevertheless, they do not work in version ``2019.1.0``, as ``DEBUG`` and ``PROGRESS`` cannot be imported. To access this variables you have to import ``LogLevel`` (``from dolfin.cpp.log import LogLevel``) and then access the desired variables (e.g. ``LogLevel.DEBUG``). Alternatively , you can use an integer, being ``log_level=10`` and ``log_level=16``, for ``DEBUG`` and ``PROGRESS``, respectively. Other options can be found `here <https://bitbucket.org/fenics-project/dolfin/src/master/dolfin/log/LogLevel.h>`_). I could tell you what each level prints, but why wouldn't try it yourself? (not in a ``jupyter notebook``!)







