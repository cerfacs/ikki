

.. |fenics| replace:: FEniCS
.. _fenics : https://fenicsproject.org/



Useful references
=================


|fenics|_ documentation is great, but scattered.


Here, the references we find the most relevant:

* Langtangen's `2017 book <https://books.google.fr/books/about/Solving_PDEs_in_Python.html?id=JtpCDwAAQBAJ&source=kp_book_description&redir_esc=y>`_ is a great starting point. 


* `Logg's book <https://fenicsproject.org/book/>`_ presents detailed information about implementation.


*  `UFL documentation <https://fenics.readthedocs.io/projects/ufl/en/latest/manual.html>`_ is very helpful to learn about variational form.


* |fenics| `discourse <https://fenicsproject.discourse.group/>`_ is the place to go for more advanced issues.


* Langtangen's `"Numerical solution of PDEs" course <http://hplgit.github.io/num-methods-for-PDEs/doc/pub/index.html>`_ contains very nice expositions of the most important concepts. (In fact, everything coming from Langtangen is very insightful and worth reading.)


* `Numerical tours of continuum mechanics using FEniCS <https://comet-fenics.readthedocs.io/en/latest/index.html#>`_, from Jeremy Bleyer, shows how to solve several Solid Mechanics problems in |fenics|. Even though these are not the problems we are concerned with at CERFACS, there's a huge overlap from which we can profit from.




Application-oriented references (specially CFD-related):


* Mikael Mortensen et al., 2011, `A FEniCS-based programming framework for modeling turbulent flow by the Reynolds-averaged Navier–Stokes equations <https://www.sciencedirect.com/science/article/abs/pii/S030917081100039X>`_.


* Jorgen S Dokken et al., 2020, `A multimesh finite element method for the Navier–Stokes equations based on projection methods <https://www.sciencedirect.com/science/article/abs/pii/S0045782520303145?via%3Dihub>`_.


* Bilen Emek Abali, 2017, `An accurate finite element method for the numerical solution of isothermal and incompressible flow of viscous fluid <https://arxiv.org/abs/1709.00913>`_.


* Jacob M Makjaars et al., 2021, `LEoPart: A particle library for FEniCS <https://www.sciencedirect.com/science/article/pii/S089812212030170X>`_.


* Qiming Zhu et al., 2021, `A moving-domain CFD solver in FEniCS with applications to tidal turbine simulations in turbulent flows <https://www.sciencedirect.com/science/article/abs/pii/S0898122119303906>`_.


* A J Otto et al., 2012, `Using the FEniCS Package for FEM Solutions in Electromagnetics <https://ieeexplore.ieee.org/document/6309184>`_.


On the HPC side:

* Johan Hoffman et al., 2016, `FEniCS-HPC: Automated Predictive High-Performance Finite Element Computing with Applications in Aerodynamics <https://link.springer.com/chapter/10.1007/978-3-319-32149-3_34>`_.


On the more implementation-related side:

* M E Rognes et al., 2013, `Automating the solution of PDEs on the sphere and other manifolds in FEniCS 1.2 <https://gmd.copernicus.org/articles/6/2099/2013/>`_.

